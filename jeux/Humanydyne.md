# Humanydyne
Humanydyne est un JDR français que l'on doit à Willy Favre et Julien Heylbroeck.

Il s'agit d'un jeu de super-héros empruntant à la culture comics pour en tirer un univers original et cohérent.

Les personnages y endossent le rôle d'agents travaillant pour Humanydyne, un organisme intervenant sur des affaires impliquant d'une façon ou d'une autre des personnes dotées de pouvoirs.

Le tout est dans un premier temps centré sur San Sepulcro, capitale de l'état mutant Madreselva. Dans cette ville bigarrée où les masques (individus dotés de pouvoirs) sont légion, les personnages vont, au fil des enquêtes, lever le voile sur les mystères qui ont conduit au monde tel qu'il est dans Humanydyne.

La gamme débute par un livre de base complet vendu avec un écran chez le 7eme Cercle. En plus de l'univers et du système, on y trouve un scénario 0 reprenant certains jalons importants de l'historique ainsi que les deux premiers scénarii de la campagne. Face au succès de cet ouvrage, un supplément papier est sorti portant sur les Nouveaux Russes. Accompagné de scénarii, il présente bien sûr la Russie mais apporte également des précisions sur le Temps du Rêve, ce territoire surnaturel où excellent les shamans.

Les autres suppléments sont édités en pdf. En plus d'un scénario de découverte (Poursuite) et de la suite de la campagne (scénario 3 : Monstrous Soul) apportant la notion d'interludios (gestion des interventions des agents), on trouve le supplément de contexte Wonderlondres. 

Vous trouverez la fiche complète sur Le Grog : [https://www.legrog.org/jeux/humanydyne](https://www.legrog.org/jeux/humanydyne)

## Wild Things
Ce supplément vous présente en détail la mangrove de Madreselva. 

Pour la petite histoire, cet ouvrage est constitué de textes que j'avais initialement rédigés pour le prochain supplément officiel (Calavera Chronicles). Celui-ci n'ayant jamais vu le jour, il aurait été dommage de ne pas donner suite. Les textes furent donc étoffés, un scénario avec une thématique proche rajouté et un autre scénario inédit rédigé pour l'occasion.

Après une visite de la mangrove, ses occupants et lieux notables sont abordés, y compris une faune bien particulière. 

Suivent deux scénarios :
* Spiders, précédemment publié dans les Carnets de l'Assemblée,
* Sauvagerie, inédit.

La mise en page a été réalisée par Tom Artigue-Cazcarra qui réalise de plus quelques illustrations en plus de celles généreusement proposées par Gilles Etienne. 

* [Télécharger le PDF Wild Things - 6.4 Mo](/assets/humanydyne/Mangrove-2.1_compressed.pdf)

## Wonderlondres

J'ai eu la chance d'écrire pour ce supplément en compagnie de Willy Favre. Le devenir de Londres y est décrit en détail. Celle-ci a été frappé par des débris de la Lune issus du combat titanesque y ayant pris place. Depuis, la capitale a été isolée du reste du monde par une grande enceinte et des dispositifs assurant que rien n'y entre ou en sorte, en faisant presque un état indépendant pour les masques qui y sont largement majoritaires. Dans une ambiance proche de New York 1997 s'y trament des intrigues auxquels les personnages pourront malgré tout prendre part et qui s'avéreront liées au déroulement de la campagne. On trouve donc dans ce supplément deux scénarii : un loner permettant une excursion dans la ville mais aussi le scénario 4 de la campagne.

*Difficile aujourd'hui de récupérer ce supplément (il fait partie des contenus qui ont sombré dans les limbes suite à la disparition du site de Brain.Salad). Mais ne perdons pas espoir...*

## Humanyzyne

Peu après la sortie du jeu apparaît un espace lui étant dédié. A l'initiative du Ludiste (qui s'appelait encore Kube à l'époque), un site appelé San Sepulcro Confidencial voit le jour (il a depuis disparu dans les limbes du web). La vocation était de présenter du matériel pour Humanydyne, que ce soit sous la forme de FAQ, de PNJ, d'interludios ou même de scénarii entiers.

C'est ainsi que j'ai pu mettre à disposition divers textes pour ce jeu.

Au niveau du contexte supplémentaire, on trouvait la Famille Johnson. Empruntant à Tom Strong et aux Quatre Fantastiques, cette famille hétéroclite part régulièrement en exploration dans le Temps du Rêve. C'est l'occasion, par le biais de ces articles, de faire découvrir diverses terres alternatives.

On y trouvait également des scénarii de mon cru. Halloween à San Sepulcro se veut un hommage aux séries B et Z (donc indirectement à Brain Soda). A Coeur Ouvert et Justice Aveugle sont les deux premiers volets d'une trilogie confrontant les personnages à un ancien dieu. 

Finalement, par le biais du forum du 7eme Cercle dédié à ce jeu un fanzine électronique vit le jour : Humanyzyne. Mené par Stéphane "L'Histrion" Slama (aidé par un groupe de fans motivés), cette publication reprit dans un premier temps des articles de San Sepulcro Confidencial en les arrangeant au passage (illustrations, mise en page).

Le premier numéro était consacré aux scénarii. Le second se penchait sur les personnages (exemples, aides de jeux, scénario...). Des suites étaient prévues mais le manque de temps a fini par prendre le dessus (comme souvent).

Les numéros de ce zine étant très difficiles à trouver, je vous les propose ici. 
Le tout est illustré et mis en page par Stéphane Slama, en plus d'illustrations par Matthieu Destephe.

* [Télécharger le PDF Humanyzyne n°1 - 1.7 Mo](/assets/humanydyne/Humanyzyne1_compressed.pdf)
* [Télécharger le PDF Humanyzyne n°2 - 1.6 Mo](/assets/humanydyne/Humanyzyne2_compressed.pdf)

## Zombie Inside

A l'origine du premier hors-série, on trouve Matthieu "Myrkvid" Destephe et moi. Inspiré de Marvel Zombies et plus généralement d'une passion contre nature pour les morts-vivants (là, ça doit commencer à se voir), ce supplément de contexte offre différents éléments pour les utiliser dans Humanydyne.

Au menu :

- une Terre Alternative où se propage un virus de zombification,

- les masques de cette Terre aux prises avec les zombies,

- des points de règles supplémentaires pour gérer la zombification (et en particulier la traduction de sa propagation par des interludios).

- une campagne partant de l'origine de l'épidémie jusqu'à une fin ouverte.
  
Vous pouvez le récupérer ici : 

* [Télécharger le PDF Zombie Inside - 1.5 Mo](/assets/humanydyne/Zombie_inside_compressed.pdf)